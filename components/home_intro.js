import Carousel from "react-material-ui-carousel";
import brokenGround from "../assets/css/home_intro.module.css";
import { Grid, Typography } from "@material-ui/core/";
import Button from "@material-ui/core/Button";

const bg = () => {
  return (
    <section className={brokenGround["p-takeover--k8s"]}>
      <Grid
        container
        spacing={0}
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid item xs={12} sm={6} md={4}>
          <div className="relative-podition">
            <div className={brokenGround["slider-box"]}>
              <CakePictureCarousel />
            </div>
          </div>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          {/* <p className="ma-0 text-center text-weight-light">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum, sed
            repellat? Quod quos suscipit itaque porro neque iusto officia
            repudiandae.
          </p> */}
          <Typography variant="h2" component="h2" gutterBottom>
            Bam-Bam Lala <br /> Special Offer
          </Typography>
          <Typography variant="body1" gutterBottom>
            Get bambam for loved ones <br /> this session!
          </Typography>

          <Button size="large" variant="contained" color="primary">
            Shop
          </Button>
        </Grid>
      </Grid>
    </section>
  );
};

const CakePictureCarousel = () => {
  return (
    <Carousel
      autoPlay={false}
      timer={true}
      className="home-into-carosel"
      animation="fade"
      indicators={true}
      timeout={500}
      navButtonsAlwaysVisible={false}
    >
      {[1, 2, 3, 4, 5].map((v, i) => {
        return (
          <img
            style={{
              position:'relative',
              left:'10px',
              top:'10px',
              height:'auto',
              width:'465px'
            }}
            className="carosel-pic-item"
            key={i + v}
            alt="Quasar logo"
            src="/images/cake.png"
          />
        );
      })}
    </Carousel>
  );
};
export default bg;
