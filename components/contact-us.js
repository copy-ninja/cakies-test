import style from "../assets/css/contact-us.module.css";
import Pointer from "../assets/svg/pointer.svg";
import { Typography } from "@material-ui/core/";

const ContactUs = () => {
  return (
    <div>
      <Typography align="center" variant="h3" component="h3" gutterBottom>
        Contact us
      </Typography>

      <div className="text-center">
        <Pointer />
      </div>
      <div>
        {" "}
        <img
          src="/images/map.png"
          className={[style["map"], style["shadow-4"]].join(" ")}
          alt="my image"
        />
      </div>
    </div>
  );
};

export default ContactUs;
