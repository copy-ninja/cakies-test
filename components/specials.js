import Pointer from "../assets/svg/pointer.svg";
import Carousel from "react-material-ui-carousel";
// import React, { Component } from "react";
import autoBind from "auto-bind";

// import { Paper } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";

import style from "../assets/css/specials.module.css";

import { Paper, Button, Typography } from "@material-ui/core";

const useStyles = (theme) => ({
  root: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  large: {
    width: theme.spacing(20),
    height: theme.spacing(20),
  },
});

function Project(props) {
  const clas = useStyles;
  console.log();
  return (
    <div>
      <Avatar
        alt="Remy Sharp"
        style={{
          width: "150px",
          height: "150px",
        }}
        src="/images/cake.png"
      />
      <Typography align="center" variant="subtitle1">
        {props.item.name}
      </Typography>
      <Typography align="center" variant="subtitle2">
      {String.fromCharCode(8358)} {props.item.price}
      </Typography>
    </div>
  );
}
function reStructureArrayForCarousel() {
  var newArray = [[]];
  var init = 0;
  for (var i = 0; i < items.length; i++) {
    if (newArray[init]) {
      if (newArray[init].length < 6 /*max items per slide*/) {
        newArray[init].push(items[i]);
      } else {
        i--;
        init++;
      }
    } else {
      newArray.push([]);
      i--;
    }
  }
  return newArray;
}
const items = [
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake ",
    price: "23,800",
    color: "#64ACC8",
  },
  {
    name: "Ginger cake",
    price: "23,800",
    color: "#64ACC8",
  },
];

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      autoPlay: false,
      timer: 500,
      animation: "fade",
      indicators: true,
      timeout: 500,
      navButtonsAlwaysVisible: false,
    };

    autoBind(this);
  }

  render() {
    const { classes } = this.props;

    // const { classes } = this.props;
    // const classes = useStyles();

    return (
      <div
        style={{
          marginTop: "50px",
          color: "#494949",
          padding: "20px 5px",
          height: "400px",
          background: "black",
        }}
      >
        <div className="text-white py-5">
          <Typography align="center" variant="h3">
            Specials
          </Typography>
          <div
            className="text-center"
            style={{
              marginTop: "15px",
            }}
          >
            <Pointer />
          </div>
        </div>

        <div
          style={{
            position: "relative",
            top: "50%",
            transform: "translateY(-70%)",
          }}
        >
          {" "}
          <Carousel
            className="special-carosel"
            autoPlay={this.state.autoPlay}
            timer={this.state.timer}
            animation={this.state.animation}
            indicators={this.state.indicators}
            timeout={this.state.timeout}
            navButtonsAlwaysVisible={this.state.navButtonsAlwaysVisible}
          >
            {reStructureArrayForCarousel().map((items, index) => {
              return (
                <Grid
                  key={index + "o"}
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                  spacing={1}
                >
                  {items.map((item, i) => {
                    return (
                      <Grid
                        key={i + "f"}
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                        className="special-carosel-item"
                        item
                        xs={4}
                        md={2}
                        spacing={3}
                      >
                        <Project item={item} sty={classes} key={i} />
                      </Grid>
                    );
                  })}
                </Grid>
              );
            })}
          </Carousel>
        </div>
      </div>
    );
  }
}
export default withStyles(useStyles)(App);
