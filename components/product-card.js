import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  rating: {
    display: "flex",
    flexDirection: "column",
    "& > * + *": {
      marginTop: theme.spacing(1),
    },
  },
}));

export default function productCard() {
  const classes = useStyles();

  return (
    // <Card
    //   style={{
    //     maxWidth: 345,
    //     marginBottom:'10px'
    //   }}
    // >
    //   <CardActionArea>
    //     <CardMedia
    //       style={{
    //         height: 140,
    //       }}
    //       image="/images/cake.png"

    //       title="Contemplative Reptile"
    //     />
    //     <CardContent>
    //       <Typography gutterBottom variant="h5" component="h2">
    //         Vanilla flavour ginger cake
    //       </Typography>
    //       <Typography variant="body2" color="textSecondary" component="p">
    //         {String.fromCharCode(8358)} 22,000
    //       </Typography>
    //     </CardContent>
    //   </CardActionArea>
    //   <CardActions>
    //     <div
    //       style={{
    //         display: "flex",
    //         flexDirection: "column",
    //         marginTop:'10px'
    //       }}
    //     >
    //       <Rating name="size-small" defaultValue={2} size="small" />
    //     </div>
    //   </CardActions>
    // </Card>
    <Box pr={2} mb={2}>
      <img
        style={{ width: "100%", height: 200}}
        alt="Vanilla flavour ginger cake"
        src="/images/cake.png"
      />

      <Typography gutterBottom variant="body2">
        Vanilla flavour ginger cake
      </Typography>
      <Typography display="block" variant="caption" color="textSecondary">
        {String.fromCharCode(8358)} 22,000
      </Typography>
      {/* <Typography variant="caption" color="textSecondary">
        {`${item.views} • ${item.createdAt}`}
      </Typography> */}
    </Box>
  );
}
